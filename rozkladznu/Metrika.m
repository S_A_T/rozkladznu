//
//  Metrika.m
//  rozkladznu
//
//  Created by Artem Shevchenko on 22.10.15.
//  Copyright © 2015 MITK. All rights reserved.
//

#import "Metrika.h"
#import "YMMYandexMetrica.h"

@implementation Metrika

+(void) reportGroupChange:(NSString *) department group:(NSString *) group
{
    NSLog(@"ReportGroupChange");
    NSDictionary *params = @{@"Факультет": department, @"Группа": group};
    [YMMYandexMetrica reportEvent:@"Смена группы"
                       parameters:params
                        onFailure:^(NSError *error) {
                            NSLog(@"error: %@", [error localizedDescription]);
                        }];
}

+(void) reportGroupAdd:(NSString *) department group:(NSString *) group
{
    NSLog(@"ReportGroupAdd");
    NSDictionary *params = @{@"Факультет": department, @"Группа": group};
    [YMMYandexMetrica reportEvent:@"Добавлена группа"
                       parameters:params
                        onFailure:^(NSError *error) {
                            NSLog(@"error: %@", [error localizedDescription]);
                        }];
}

+(void) reportGroupAdd:(NSString *) department group:(NSString *) group subgroup:(NSString *) subgroup
{
    NSLog(@"ReportGroupAdd");
    NSDictionary *params = @{@"Факультет": department, @"Группа": group, @"Подгруппа": subgroup};
    [YMMYandexMetrica reportEvent:@"Добавлена группа"
                       parameters:params
                        onFailure:^(NSError *error) {
                            NSLog(@"error: %@", [error localizedDescription]);
                        }];
}

@end
