//
//  Constants.m
//  rozkladznu
//
//  Created by Artem Shevchenko on 04.09.15.
//  Copyright (c) 2015 MITK. All rights reserved.
//

#import "Constants.h"

NSString * const GROUP_URL = @"http://rozkladznu.pp.ua/api/v1/group/?format=json";
NSString * const DEPARTMENT_URL = @"http://rozkladznu.pp.ua/api/v1/department/?format=json";
NSString * const TIMETABLE_URL = @"http://rozkladznu.pp.ua/api/v1/timetable/?group=";
NSString * const SUBJECTS_URL = @"http://rozkladznu.pp.ua/api/v1/lesson/?format=json";
NSString * const TIME_URL = @"http://rozkladznu.pp.ua/api/v1/time/?format=json";
NSString * const AUDIENCE_URL = @"http://rozkladznu.pp.ua/api/v1/audience/?format=json";
NSString * const CAMPUS_URL = @"http://rozkladznu.pp.ua/api/v1/campus/?format=json";
NSString * const TEACHER_URL = @"http://rozkladznu.pp.ua/api/v1/teacher/?format=json";
NSString * const CURRENT_WEEK = @"http://rozkladznu.pp.ua/api/v1/current_week/?format=json";


@implementation Constants

@end
