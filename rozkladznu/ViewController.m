//
//  ViewController.m
//  rozkladznu
//
//  Created by Artem Shevchenko on 04.09.15.
//  Copyright (c) 2015 MITK. All rights reserved.
//

#import "ViewController.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "RozkladController.h"
#import "UIView+Toast.h"
#import "Metrika.h"



@interface ViewController ()
        @property (strong) NSDictionary *contentDict;

@end



@implementation ViewController
{
    NSString *selectedDepartment;
    int selectedDepartmentId;
    int selectedGroupId;
    BOOL canSelect;
    BOOL isGroupChange;
}
enum Action
{
    SELECT_DEPARTMENT = 1,
    SELECT_GROUP = 2
};

enum Action currentAction = SELECT_DEPARTMENT;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    currentAction = SELECT_DEPARTMENT;
    [self LoadData: DEPARTMENT_URL];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isGroupChange = NO;
    if([[defaults objectForKey:@"go_to_select_group"] isEqualToString:@"YES"])
    {
        isGroupChange = YES;
    }
    [defaults setObject:@"NO" forKey:@"go_to_select_group"];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(currentAction == SELECT_DEPARTMENT)
    {
        self.navigationItem.hidesBackButton = NO;
        self.navigationItem.leftBarButtonItem = nil;
        return @"Оберіть факультет";
    }
    else
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStyleDone target:self action:@selector(RefreshSelector)];
        self.navigationItem.hidesBackButton = YES;
        return selectedDepartment;
    }
}

-(void) RefreshSelector
{
    [self LoadData: DEPARTMENT_URL];
    currentAction = SELECT_DEPARTMENT;
}

- (void)LoadData:(NSString *) myUrl
{
    [self.navigationController.view makeToastActivity];
    canSelect = NO;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:myUrl parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [self.navigationController.view hideToastActivity];
              self.contentDict = (NSMutableDictionary *)responseObject;
              NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [self.tableView numberOfSections])];
              [self.tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
              canSelect = YES;
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [self.navigationController.view hideToastActivity];
              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Помилка отримання даних"
                                                                  message:[error localizedDescription]
                                                                 delegate:nil
                                                        cancelButtonTitle:@"Добре"
                                                        otherButtonTitles:nil];
              [alertView show];
          }];
}

- (IBAction)Refresh:(id)sender {
    [self LoadData: DEPARTMENT_URL];
    currentAction = SELECT_DEPARTMENT;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contentDict[@"objects"] count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Facultet_Cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Facultet_Cell"];
    }
    
    [(UILabel *)[cell viewWithTag:1000] setText:self.contentDict[@"objects"][indexPath.row][@"name"]];
    
    return cell;
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (canSelect){
        if(currentAction == SELECT_DEPARTMENT)
        {
            selectedDepartment = self.contentDict[@"objects"][indexPath.row][@"name"];
            selectedDepartmentId = [self.contentDict[@"objects"][indexPath.row][@"id"] intValue];
            [self LoadData:[GROUP_URL stringByAppendingString:[NSString stringWithFormat:@"&department=%d",selectedDepartmentId]]];
            currentAction = SELECT_GROUP;
        }
        else if(currentAction == SELECT_GROUP)
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"YES" forKey:@"group_is_selected"];
            [defaults setObject:self.contentDict[@"objects"][indexPath.row][@"id"] forKey:@"selected_group"];
            [defaults setObject:self.contentDict[@"objects"][indexPath.row][@"name"] forKey:@"selected_group_name"];
            
            [defaults setObject:self.contentDict[@"objects"][indexPath.row][@"subgroup_count"] forKey:@"subgroup_count"];
            [defaults setObject:@"0" forKey:@"subgroup"];
            
            if(isGroupChange)
                [Metrika reportGroupChange:selectedDepartment group:self.contentDict[@"objects"][indexPath.row][@"name"]];
            else
                [Metrika reportGroupAdd:selectedDepartment group:self.contentDict[@"objects"][indexPath.row][@"name"]];
            
            [self performSegueWithIdentifier:@"TO_ROZKLAD" sender:nil];
        }
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



@end
