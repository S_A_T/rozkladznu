//
//  MenuController.m
//  rozkladznu
//
//  Created by Artem Shevchenko on 12.09.15.
//  Copyright (c) 2015 MITK. All rights reserved.
//

#import "MenuController.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "RozkladController.h"

@implementation MenuController{
    NSArray *menuTitles;
    NSArray *menuImages;
    int curWeek;
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    [self.indicator startAnimating];
    [self.indicator setHidden:YES];
//    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:@"current_week"] intValue] == 1)
    {
        [(UILabel *)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] viewWithTag:999] setText:@"Чисельник"];
    }
    else if([[defaults objectForKey:@"current_week"] intValue] == 2)
    {
        [(UILabel *)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] viewWithTag:999] setText:@"Знаменник"];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // Force your tableview margins (this may be a bad idea)
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Оберіть тиждень" message:@"Оберіть розклад для якого тиждня ви хочете бачити." delegate:self cancelButtonTitle:@"Залишити як є" otherButtonTitles:@"Чисельник", @"Знаменник", @"Поточний", nil];
        [alert show];
    }
    else if(indexPath.row == 1)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Оберіть підгрупу" message:nil delegate:self cancelButtonTitle:@"Залишити як є" otherButtonTitles: nil];
        for(int i = 0; i < [[defaults objectForKey:@"subgroup_count"] intValue]; i++)
        {
            [alert addButtonWithTitle:[NSString stringWithFormat:@"Підгруппа %i",i+1]];
        }
        [alert show];
    }
    else if(indexPath.row == 2)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"NO" forKey:@"group_is_selected"];
        [defaults setObject:@"YES" forKey:@"delete_from_timetable"];
        [defaults setObject:@"YES" forKey:@"go_to_select_group"];
    }
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([alertView.title isEqualToString:@"Оберіть тиждень"]){
        if(buttonIndex == 1)
        {
            [defaults setObject:@"1" forKey:@"current_week"];
            [(UILabel *)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] viewWithTag:999] setText:@"Чисельник"];
            [defaults setObject:@"NO" forKey:@"get_current_week"];
        }
        else if(buttonIndex == 2)
        {
            [defaults setObject:@"2" forKey:@"current_week"];
            [(UILabel *)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] viewWithTag:999] setText:@"Знаменник"];
            [defaults setObject:@"NO" forKey:@"get_current_week"];
        }
        else if(buttonIndex == 3)
        {
            [self GetCurrentWeek];
        }
    }
    else if ([alertView.title isEqualToString:@"Оберіть підгрупу"])
    {
        if(buttonIndex != 0)
            [defaults setObject:[NSNumber numberWithInteger:buttonIndex] forKey:@"subgroup"];
    }
}

-(void) GetCurrentWeek
{
    [self.indicator setHidden:NO];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:CURRENT_WEEK parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:(NSMutableDictionary *)responseObject[@"timetable_week"] forKey:@"current_week"];
             [defaults setObject:@"YES" forKey:@"get_current_week"];
             if([responseObject[@"timetable_week"] intValue] == 1)
             {
                 [(UILabel *)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] viewWithTag:999] setText:@"Чисельник"];
             }
             else
             {
                 [(UILabel *)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] viewWithTag:999] setText:@"Знаменник"];
             }
             [self.indicator setHidden:YES];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Помилка отримання даних"
                                                                 message:[error localizedDescription]
                                                                delegate:nil
                                                       cancelButtonTitle:@"Добре"
                                                       otherButtonTitles:nil];
             [alertView show];
             [self.indicator setHidden:YES];
         }];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end
