//
//  MainMenu.h
//  rozkladznu
//
//  Created by Artem Shevchenko on 10.09.15.
//  Copyright (c) 2015 MITK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenu : UIViewController <UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UILabel *TitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *DescrLabel;
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong) NSDictionary *contentDict;

@end
