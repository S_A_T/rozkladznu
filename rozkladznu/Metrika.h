//
//  Metrika.h
//  rozkladznu
//
//  Created by Artem Shevchenko on 22.10.15.
//  Copyright © 2015 MITK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Metrika : NSObject

+(void) reportGroupChange:(NSString *) department group:(NSString *) group;
+(void) reportGroupAdd:(NSString *) department group:(NSString *) group;
+(void) reportGroupAdd:(NSString *) department group:(NSString *) group subgroup:(NSString *) subgroup;

@end
