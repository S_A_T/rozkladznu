//
//  ViewController.h
//  rozkladznu
//
//  Created by Artem Shevchenko on 04.09.15.
//  Copyright (c) 2015 MITK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;


@end

