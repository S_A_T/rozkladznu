//
//  Constants.h
//  rozkladznu
//
//  Created by Artem Shevchenko on 04.09.15.
//  Copyright (c) 2015 MITK. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const GROUP_URL;
extern NSString * const DEPARTMENT_URL;
extern NSString * const TIMETABLE_URL;
extern NSString * const SUBJECTS_URL;
extern NSString * const TIME_URL;
extern NSString * const AUDIENCE_URL;
extern NSString * const CAMPUS_URL;
extern NSString * const TEACHER_URL;
extern NSString * const CURRENT_WEEK;

@interface Constants : NSObject


@end
