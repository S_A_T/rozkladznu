//
//  RozkladController.m
//  rozkladznu
//
//  Created by Artem Shevchenko on 12.09.15.
//  Copyright (c) 2015 MITK. All rights reserved.
//

#import "RozkladController.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "FMDB.h"
#import "UIView+Toast.h"

@implementation RozkladController
{
    int group_id;
    int week_type;
    int subgroup_id;
    FMDatabase *database;
    FMResultSet *rs;
    NSMutableArray *subjectsArray;
    int todatIs;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    subjectsArray = [[NSMutableArray alloc] init];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    group_id = [[defaults objectForKey:@"selected_group"] intValue];
    self.navigationItem.title = [defaults objectForKey:@"selected_group_name"];
    
    NSString *databaseName = @"rozklad_db";
    
    NSString *documentsFolder          = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *databaseFullDocumentPath = [documentsFolder stringByAppendingPathComponent:databaseName];

    database = [[FMDatabase alloc] initWithPath:databaseFullDocumentPath];
    [database open];
    rs = [database executeQuery:@"select count(*) from `timetable`;"];
    [rs next];
    if([[rs stringForColumnIndex:0] intValue] == 0){
        [self.navigationController.view makeToastActivity];
        if([[defaults objectForKey:@"get_current_week"] isEqualToString:@"NO"] )
        {
            [self LoadData:TIMETABLE_URL];
        }
        else
        {
            [self GetCurrentWeek];
        }
    }
    else
    {
        double delayInSeconds = 0.55f;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self.tableView reloadData];
            [self PullTableViewToCurrentDay];
        });
    }
}



-(void) viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    subgroup_id = [[defaults objectForKey:@"subgroup"] intValue];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:27.0f/255.0f green:125.0f/255.0f blue:217.0f alpha:1.0f]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (IBAction)Refresh:(id)sender {
    [self LoadData:TIMETABLE_URL];
}
-(void) GetCurrentWeek
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:CURRENT_WEEK parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:(NSMutableDictionary *)responseObject[@"timetable_week"] forKey:@"current_week"];
             [defaults setObject:@"YES" forKey:@"get_current_week"];
             [self LoadData:TIMETABLE_URL];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Помилка отримання даних"
                                                                 message:[error localizedDescription]
                                                                delegate:nil
                                                       cancelButtonTitle:@"Добре"
                                                       otherButtonTitles:nil];
             [alertView show];
         }];
}
- (void)LoadData:(NSString *) myUrl
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%i%@", myUrl, group_id, @"&format=json"] parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             self.contentDict = (NSMutableDictionary *)responseObject;
             [self LoadTimetableToDatabase];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Помилка отримання даних"
                                                                 message:[error localizedDescription]
                                                                delegate:nil
                                                       cancelButtonTitle:@"Добре"
                                                       otherButtonTitles:nil];
             [alertView show];
             [self.navigationController.view hideToastActivity];
             [self.tableView reloadData];
         }];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    int count = 7;
    [self.tableView setRowHeight:73];
    rs = [database executeQuery:@"select count(*) from `timetable`;"];
    [rs next];
    int countOfElements = [[rs stringForColumnIndex:0] intValue];
    if(self.contentDict != nil){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        subgroup_id = [[defaults objectForKey:@"subgroup"] intValue];
        NSLog(@"%@", [defaults objectForKey:@"subgroup"]);
        [subjectsArray removeAllObjects];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        BOOL loadFromDB = NO;
        if(countOfElements > 0)
            loadFromDB = YES;
        rs = [database executeQuery:[NSString stringWithFormat:@"SELECT * FROM `timetable`;"]];
        NSDictionary *dict;
        
        NSArray *keys = [[NSArray alloc] initWithObjects:@"audience_id",
                         @"date_end",
                         @"date_start",
                         @"day",
                         @"id",
                         @"lesson_id",
                         @"lesson_type",
                         @"periodicity",
                         @"subgroup",
                         @"teacher_id",
                         @"time_id", nil];
        if(loadFromDB){
            for(int i = 0; i < countOfElements; i++)
            {
                [rs next];
                NSArray *objs = [[NSArray alloc] initWithObjects:[rs stringForColumn:@"audience_id"],
                                 [rs stringForColumn:@"date_end"],
                                 [rs stringForColumn:@"date_start"],
                                 [rs stringForColumn:@"day"],
                                 [rs stringForColumn:@"id"],
                                 [rs stringForColumn:@"lesson_id"],
                                 [rs stringForColumn:@"lesson_type"],
                                 [rs stringForColumn:@"periodicity"],
                                 [rs stringForColumn:@"subgroup"],
                                 [rs stringForColumn:@"teacher_id"],
                                 [rs stringForColumn:@"time_id"], nil];
                dict = [[NSDictionary alloc] initWithObjects:objs forKeys:keys];
                if(subgroup_id == [dict[@"subgroup"] intValue] || [dict[@"subgroup"] intValue] == 0 || subgroup_id == 0)
                {
                    if([[defaults objectForKey:@"current_week"] intValue] == [dict[@"periodicity"] intValue] || [dict[@"periodicity"] intValue] == 0 || [[defaults objectForKey:@"current_week"] intValue] == 0){
                        [[subjectsArray objectAtIndex:[dict[@"day"] intValue]] addObject:dict];
                    }
                }

            }
        }
        else
        {
            for(int i = 0; i < [self.contentDict[@"objects"] count]; i++)
            {
                if(subgroup_id == [self.contentDict[@"objects"][i][@"subgroup"] intValue] || [self.contentDict[@"objects"][i][@"subgroup"] intValue] == 0 || subgroup_id == 0)
                {
                    if([[defaults objectForKey:@"current_week"] intValue] == [self.contentDict[@"objects"][i][@"periodicity"] intValue] || [self.contentDict[@"objects"][i][@"periodicity"] intValue] == 0 || [[defaults objectForKey:@"current_week"] intValue] == 0){
                        [[subjectsArray objectAtIndex:[self.contentDict[@"objects"][i][@"day"] intValue]] addObject:self.contentDict[@"objects"][i]];
                    }
                }
            }
        }
        for (int i = 6; i >= 0; i--) {
            if(!([[subjectsArray objectAtIndex:i] count] > 0))
            {
                count--;
            }
            else
            {
                break;
            }
        }
        for(int i = 0; i < 7; i++)
        {
            if([[subjectsArray objectAtIndex:i] count] > 1)
            {
                NSSortDescriptor *sortDescriptor;
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"time_id"
                                                             ascending:YES];
                NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                NSArray *sortedArray;
                sortedArray = [[subjectsArray objectAtIndex:i] sortedArrayUsingDescriptors:sortDescriptors];
                subjectsArray[i] = sortedArray;
                }
        }
        
    }
    else if(countOfElements > 0)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        subgroup_id = [[defaults objectForKey:@"subgroup"] intValue];
        NSLog(@"%@", [defaults objectForKey:@"subgroup"]);
        [subjectsArray removeAllObjects];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        [subjectsArray addObject:[[NSMutableArray alloc] init]];
        BOOL loadFromDB = NO;
        
        if(countOfElements > 0)
            loadFromDB = YES;
        rs = [database executeQuery:[NSString stringWithFormat:@"SELECT * FROM `timetable`;"]];
        NSDictionary *dict;
        
        NSArray *keys = [[NSArray alloc] initWithObjects:@"audience_id",
                         @"date_end",
                         @"date_start",
                         @"day",
                         @"id",
                         @"lesson_id",
                         @"lesson_type",
                         @"periodicity",
                         @"subgroup",
                         @"teacher_id",
                         @"time_id", nil];
        if(loadFromDB){
            for(int i = 0; i < countOfElements; i++)
            {
                [rs next];
                NSArray *objs = [[NSArray alloc] initWithObjects:[rs stringForColumn:@"audience_id"],
                                 [rs stringForColumn:@"date_end"],
                                 [rs stringForColumn:@"date_start"],
                                 [rs stringForColumn:@"day"],
                                 [rs stringForColumn:@"id"],
                                 [rs stringForColumn:@"lesson_id"],
                                 [rs stringForColumn:@"lesson_type"],
                                 [rs stringForColumn:@"periodicity"],
                                 [rs stringForColumn:@"subgroup"],
                                 [rs stringForColumn:@"teacher_id"],
                                 [rs stringForColumn:@"time_id"], nil];
                dict = [[NSDictionary alloc] initWithObjects:objs forKeys:keys];
                if(subgroup_id == [dict[@"subgroup"] intValue] || [dict[@"subgroup"] intValue] == 0 || subgroup_id == 0)
                {
                    if([[defaults objectForKey:@"current_week"] intValue] == [dict[@"periodicity"] intValue] || [dict[@"periodicity"] intValue] == 0 || [[defaults objectForKey:@"current_week"] intValue] == 0){
                        [[subjectsArray objectAtIndex:[dict[@"day"] intValue]] addObject:dict];
                    }
                }
                
            }
            
            for (int i = 6; i >= 0; i--) {
                if(!([[subjectsArray objectAtIndex:i] count] > 0))
                {
                    count--;
                }
                else
                {
                    break;
                }
            }
            for(int i = 0; i < 7; i++)
            {
                if([[subjectsArray objectAtIndex:i] count] > 1)
                {
                    NSSortDescriptor *sortDescriptor;
                    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"time_id"
                                                                 ascending:YES];
                    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                    NSArray *sortedArray;
                    sortedArray = [[subjectsArray objectAtIndex:i] sortedArrayUsingDescriptors:sortDescriptors];
                    subjectsArray[i] = sortedArray;
                }
            }
        }
    }
    else
        count = 0;
    return count;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([subjectsArray count] > 0)
        return [[subjectsArray objectAtIndex:section] count];
    else
        return 0;
}
-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return @"Понеділок";
            break;
        case 1:
            return @"Вівторок";
            break;
        case 2:
            return @"Середа";
            break;
        case 3:
            return @"Четвер";
            break;
        case 4:
            return @"П'ятниця";
            break;
        case 5:
            return @"Субота";
            break;
        case 6:
            return @"Неділя";
            break;
            
        default:
            return @"";
            break;
    }
}



- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if (section == [[[NSUserDefaults standardUserDefaults] objectForKey:@"current_weekday"] integerValue])
    {
        if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
            
            UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
            tableViewHeaderFooterView.tintColor = [UIColor colorWithRed:27.0f/255.0f green:125.0f/255.0f blue:217.0f alpha:0.75f];
            tableViewHeaderFooterView.textLabel.textColor = [UIColor whiteColor];//[UIColor colorWithRed:27.0f/255.0f green:125.0f/255.0f blue:217.0f alpha:1.0f];
        }
    }
    else
    {
        if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
            
            UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
            tableViewHeaderFooterView.textLabel.textColor = [UIColor grayColor];
            tableViewHeaderFooterView.tintColor = [UIColor colorWithWhite:0.95 alpha:0.75f];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(void) viewWillDisappear:(BOOL)animated
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:@"delete_from_timetable"] isEqualToString:@"YES"])
    {
        [defaults setObject:@"NO" forKey:@"delete_from_timetable"];
        [database executeUpdate:@"delete from `timetable`;"];
    }
    [database close];
}

-(void) LoadTimetableToDatabase
{
    [self.tableView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0)), dispatch_get_main_queue(), ^(void)
                   {
                       [database executeUpdate:@"delete from `timetable`;"];

                       for(int i = 0; i < [self.contentDict[@"meta"][@"total_count"] intValue]; i++)
                       {
                           [database executeUpdate: @"INSERT INTO `timetable` (audience_id, date_end, date_start, day, id, lesson_id, lesson_type, periodicity, subgroup, teacher_id, time_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", self.contentDict[@"objects"][i][@"audience_id"], self.contentDict[@"objects"][i][@"date_end"], self.contentDict[@"objects"][i][@"date_start"], self.contentDict[@"objects"][i][@"day"], self.contentDict[@"objects"][i][@"id"], self.contentDict[@"objects"][i][@"lesson_id"], self.contentDict[@"objects"][i][@"lesson_type"], self.contentDict[@"objects"][i][@"periodicity"], self.contentDict[@"objects"][i][@"subgroup"], self.contentDict[@"objects"][i][@"teacher_id"], self.contentDict[@"objects"][i][@"time_id"]];
                       }
                       [self.navigationController.view hideToastActivity];
                       [self.tableView reloadData];
                       [self PullTableViewToCurrentDay];
                   });
}

-(void) PullTableViewToCurrentDay
{
    NSInteger section = [[[NSUserDefaults standardUserDefaults] objectForKey:@"current_weekday"] integerValue];
    if(section < [self.tableView numberOfSections] && section > 0){
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rozkladCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"rozkladCell"];
    }
    
    [(UILabel *)[cell viewWithTag:997] setAdjustsFontSizeToFitWidth:YES];
    rs = [database executeQuery:[NSString stringWithFormat:@"SELECT * FROM `time` where id = %@", subjectsArray[indexPath.section][indexPath.row][@"time_id"]]];
    [rs next];
    
    NSString *time = [[[[rs stringForColumn:@"time_start"] substringToIndex:5] stringByAppendingString:@" - "] stringByAppendingString:[[rs stringForColumn:@"time_end"] substringToIndex:5]];
    [(UILabel *)[cell viewWithTag:997] setText:time];
    
    
    [(UILabel *)[cell viewWithTag:998] setAdjustsFontSizeToFitWidth:YES];
    rs = [database executeQuery:[NSString stringWithFormat:@"SELECT * FROM `audience` where id = %@", subjectsArray[indexPath.section][indexPath.row][@"audience_id"]]];
    [rs next];
    NSString *auditory_text = [rs stringForColumn:@"audience"];
    NSString *camp_id = [rs stringForColumn:@"campus_id"];
    rs = [database executeQuery:[NSString stringWithFormat:@"SELECT * FROM `campus` where id = %@", camp_id]];
    [rs next];
    [(UILabel *)[cell viewWithTag:998] setText:[[auditory_text stringByAppendingString:@"/"] stringByAppendingString:[rs stringForColumn:@"name"]]];
    
    

    rs = [database executeQuery:[NSString stringWithFormat:@"SELECT * FROM `subjects` where id = %@", subjectsArray[indexPath.section][indexPath.row][@"lesson_id"]]];
    [rs next];
    [(UILabel *)[cell viewWithTag:1000] setText:[rs stringForColumn:@"name"]];
    
    [(UILabel *)[cell viewWithTag:999] setAdjustsFontSizeToFitWidth:YES];
    rs = [database executeQuery:[NSString stringWithFormat:@"SELECT * FROM `teacher` where id = %@", subjectsArray[indexPath.section][indexPath.row][@"teacher_id"]]];
    [rs next];
    [(UILabel *)[cell viewWithTag:999] setText:[rs stringForColumn:@"name"]];


    return cell;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)revealController:(SWRevealViewController *)revealController animateToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft)
    {
        [self.tableView reloadData];
        [self PullTableViewToCurrentDay];
    }
}

@end
