//
//  MainMenu.m
//  rozkladznu
//
//  Created by Artem Shevchenko on 10.09.15.
//  Copyright (c) 2015 MITK. All rights reserved.
//

#import "MainMenu.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "FMDB.h"
#import "LGAlertView.h"
#import "UIView+Toast.h"

@implementation MainMenu
{
    LGAlertView *waitAlert;
    LGAlertView *updateWaitAlert;
    NSTimer *tryToLoad;
    BOOL itsTryToLoad;
    BOOL showQuestion;
    BOOL isUpdate;
}

enum Action
{
    LOAD_SUBJECTS = 1,
    LOAD_TEACHERS = 2,
    LOAD_AUDIENCE = 3,
    LOAD_CAMPUS = 4,
    LOAD_TIME = 5,
    LOAD_END = 6
};

enum Action currentLoadAction = LOAD_SUBJECTS;


-(void) viewDidLoad
{
    [super viewDidLoad];
    showQuestion = NO;
    currentLoadAction = LOAD_SUBJECTS;
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"3campus"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    // border radius
    [_button.layer setCornerRadius:_button.frame.size.height/2];
    
    float val = 1;
    
    // border
    [_button.layer setBorderColor:[UIColor colorWithRed:val green:val blue:val alpha:1].CGColor];
    [_button.layer setBorderWidth:1.0f];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:@"go_to_select_group"] isEqualToString:@"YES"] == NO){
        if([[defaults objectForKey:@"group_is_selected"] isEqualToString:@"YES"] && [self Check])
            [self performSegueWithIdentifier:@"GO_TO_ROZKLAD" sender:nil];
        else
        {
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSError *error;
            NSString *databaseName = @"rozklad_db";
            
            NSString *documentsFolder          = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *databaseFullDocumentPath = [documentsFolder stringByAppendingPathComponent:databaseName];
            
            if ([fileManager fileExistsAtPath:databaseFullDocumentPath] == YES) {
                [fileManager removeItemAtPath:databaseFullDocumentPath error:&error];
            }
            NSString *resourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: @"rozklad_db"];
            [fileManager copyItemAtPath:resourcePath toPath:databaseFullDocumentPath error:&error];

            [self LoadData:SUBJECTS_URL];
            [self.button setEnabled:NO];
            itsTryToLoad = NO;
            waitAlert = [[LGAlertView alloc] initWithProgressViewStyleWithTitle:@"Зачекайте!" message:@"Зачекайте, будь ласка, йде завантаження бази даних! \nШвидкість завантаження залежить від швидкості інтернету, але це не займе багато часу :)" progressLabelText:@"Завантаження" buttonTitles:nil cancelButtonTitle:nil destructiveButtonTitle:nil];
            [waitAlert setCancelOnTouch:NO];
        }
    }
    else
    {
        [self performSegueWithIdentifier:@"GO_TO_NEXT" sender:nil];
    }
}
-(void) SetFontSize
{
    _TitleLabel.adjustsFontSizeToFitWidth = YES;
    _DescrLabel.adjustsFontSizeToFitWidth = YES;
    _button.titleLabel.adjustsFontSizeToFitWidth = YES;
}
-(void) viewDidAppear:(BOOL)animated
{
    if(showQuestion)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-M-d"];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"Europe/Kiev"]];
        
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[dateFormat dateFromString:[defaults objectForKey:@"updated_at"]]];
        
        NSInteger up_day = [components day];
        NSInteger up_month = [components month];
        NSInteger up_year = [components year];
        
        components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
        
        
        if(up_day != [components day] || up_month != [components month] || up_year != [components year])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Оновлення даних" message:@"Доброго дня, ми пропонуємо Вам оновити базу, щоб уникнути помилок у відображенні розкладу на приладі." delegate:self cancelButtonTitle:@"Не треба" otherButtonTitles:@"Оновити", nil];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Оновлення даних" message:@"Доброго дня, ми пропонуємо Вам оновити базу. Для цього закрийте додаток, та відкрийте заново." delegate:self cancelButtonTitle:@"Добре" otherButtonTitles: nil];
            alert.tag = 1;
            [alert show];

        }
    }
    showQuestion = YES;
}
-(void) viewWillAppear:(BOOL)animated
{
    [self SetFontSize];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)LoadData:(NSString *) myUrl
{
    isUpdate = NO;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:myUrl parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             itsTryToLoad = NO;
             self.contentDict = (NSMutableDictionary *)responseObject;
             if(currentLoadAction == LOAD_SUBJECTS){
                 [waitAlert dismissAnimated:YES completionHandler:nil];
                 [waitAlert showAnimated:YES completionHandler:nil];
                 [self LoadToTable:@"subjects" withRequest:@"INSERT INTO `subjects` (id, name, teacher_id) VALUES ( ? , ?, ?);"];
             }
             else if(currentLoadAction == LOAD_TEACHERS)
                 [self LoadToTable:@"teacher" withRequest:@"INSERT INTO `teacher` (id, name) VALUES ( ? , ?);"];
             else if(currentLoadAction == LOAD_AUDIENCE)
                 [self LoadToTable:@"audience" withRequest:@"INSERT INTO `audience` (id, audience, campus_id) VALUES ( ? , ?, ?);"];
             else if(currentLoadAction == LOAD_CAMPUS)
                 [self LoadToTable:@"campus" withRequest:@"INSERT INTO `campus` (id, name) VALUES ( ? , ?);"];
             else if(currentLoadAction == LOAD_TIME)
                 [self LoadToTable:@"time" withRequest:@"INSERT INTO `time` (id, num, time_end, time_start) VALUES ( ? , ? , ? , ?);"];
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             if(!itsTryToLoad){
                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Помилка отримання даних"
                                                                     message:[error localizedDescription]
                                                                    delegate:self
                                                           cancelButtonTitle:@"Добре"
                                                           otherButtonTitles:nil];
                 [alertView show];
             }
             
             tryToLoad = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(TryToLoad) userInfo:nil repeats:NO];
         }];
}

- (void)UpdateData:(NSString *) myUrl
{
    isUpdate = YES;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:myUrl parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             itsTryToLoad = NO;
             self.contentDict = (NSMutableDictionary *)responseObject;
             if(currentLoadAction == LOAD_SUBJECTS){
                 [updateWaitAlert dismissAnimated:YES completionHandler:nil];
                 [updateWaitAlert showAnimated:YES completionHandler:nil];
                 [self LoadToTable:@"subjects" withRequest:@"INSERT INTO `subjects` (id, name, teacher_id) VALUES ( ? , ?, ?);"];
             }
             else if(currentLoadAction == LOAD_TEACHERS)
                 [self LoadToTable:@"teacher" withRequest:@"INSERT INTO `teacher` (id, name) VALUES ( ? , ?);"];
             else if(currentLoadAction == LOAD_AUDIENCE)
                 [self LoadToTable:@"audience" withRequest:@"INSERT INTO `audience` (id, audience, campus_id) VALUES ( ? , ?, ?);"];
             else if(currentLoadAction == LOAD_CAMPUS)
                 [self LoadToTable:@"campus" withRequest:@"INSERT INTO `campus` (id, name) VALUES ( ? , ?);"];
             else if(currentLoadAction == LOAD_TIME)
                 [self LoadToTable:@"time" withRequest:@"INSERT INTO `time` (id, num, time_end, time_start) VALUES ( ? , ? , ? , ?);"];
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             if(!itsTryToLoad){
                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Помилка отримання даних"
                                                                     message:[error localizedDescription]
                                                                    delegate:self
                                                           cancelButtonTitle:@"Добре"
                                                           otherButtonTitles:nil];
                 [alertView show];
             }
             
             tryToLoad = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(TryToLoad) userInfo:nil repeats:NO];
         }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        if(buttonIndex == 1)
        {
            currentLoadAction = LOAD_SUBJECTS;
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSError *error;
            NSString *databaseName = @"rozklad_db";
            
            NSString *documentsFolder          = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *databaseFullDocumentPath = [documentsFolder stringByAppendingPathComponent:databaseName];
            
            if ([fileManager fileExistsAtPath:databaseFullDocumentPath] == YES) {
                [fileManager removeItemAtPath:databaseFullDocumentPath error:&error];
            }
            NSString *resourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: @"rozklad_db"];
            [fileManager copyItemAtPath:resourcePath toPath:databaseFullDocumentPath error:&error];
            
            [self UpdateData:SUBJECTS_URL];
            [self.button setEnabled:NO];
            itsTryToLoad = NO;
            updateWaitAlert = [[LGAlertView alloc] initWithProgressViewStyleWithTitle:@"Зачекайте!" message:@"Зачекайте, будь ласка, йде завантаження бази даних! \nШвидкість завантаження залежить від швидкості інтернету, але це не займе багато часу :)" progressLabelText:@"Завантаження" buttonTitles:nil cancelButtonTitle:nil destructiveButtonTitle:nil];
            [updateWaitAlert setCancelOnTouch:NO];
        }
    }
    else
    {
        [waitAlert showAnimated:YES completionHandler:nil];
    }
}

-(void) TryToLoad
{
    itsTryToLoad = YES;
    [self LoadData:SUBJECTS_URL];
}

-(void) LoadToTable:(NSString *) table withRequest: (NSString *) request
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0)), dispatch_get_main_queue(), ^(void)
    {
        NSString *databaseName = @"rozklad_db";
        
        NSString *documentsFolder          = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *databaseFullDocumentPath = [documentsFolder stringByAppendingPathComponent:databaseName];
        
        FMDatabase *database = [[FMDatabase alloc] initWithPath:databaseFullDocumentPath];
        BOOL success = [database open];
        if (success)
        {
            [database open];
            [database executeUpdate:[NSString stringWithFormat:@"delete from %@;", table ]];
            for(int i = 0; i < [self.contentDict[@"meta"][@"total_count"] intValue]; i++)
            {
                if((self.contentDict[@"objects"][i][@"id"] != nil && self.contentDict[@"objects"][i][@"name"]) || currentLoadAction == LOAD_AUDIENCE || currentLoadAction == LOAD_TIME)
                {
                    if(currentLoadAction == LOAD_SUBJECTS)
                    {
                        [database executeUpdate: request, [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"id"]] , [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"name"]] , [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"teacher_id"]]];
                    }
                    else if(currentLoadAction == LOAD_TEACHERS)
                    {
                        [database executeUpdate: request, [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"id"]] , [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"name"]]];
                    }
                    else if(currentLoadAction == LOAD_AUDIENCE)
                    {
                        [database executeUpdate: request, [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"id"]] , [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"audience"]], [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"campus_id"]]];
                    }
                    else if(currentLoadAction == LOAD_CAMPUS)
                    {
                        [database executeUpdate: request, [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"id"]] , [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"name"]]];
                    }
                    else if(currentLoadAction == LOAD_TIME)
                    {
                        [database executeUpdate: request, [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"id"]], [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"num"]], [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"time_end"]] , [NSString stringWithFormat:@"%@",self.contentDict[@"objects"][i][@"time_start"]]];
                    }
                }
            }
        }
        [database close];
        
        
        if(isUpdate == NO){
            if(currentLoadAction == LOAD_SUBJECTS)
            {
                currentLoadAction = LOAD_TEACHERS;
                [self LoadData:TEACHER_URL];
                [waitAlert setProgress:0.20 progressLabelText:@"Предмети завантажено"];
            }
            else if(currentLoadAction == LOAD_TEACHERS)
            {
                currentLoadAction = LOAD_AUDIENCE;
                [self LoadData:AUDIENCE_URL];
                [waitAlert setProgress:0.40 progressLabelText:@"Аудиторії завантажено"];
            }
            else if(currentLoadAction == LOAD_AUDIENCE)
            {
                currentLoadAction = LOAD_CAMPUS;
                [self LoadData:CAMPUS_URL];
                [waitAlert setProgress:0.60 progressLabelText:@"Корпуса завантажено"];
            }
            else if(currentLoadAction == LOAD_CAMPUS)
            {
                currentLoadAction = LOAD_TIME;
                [self LoadData:TIME_URL];
                
                [waitAlert setProgress:0.80 progressLabelText:@"Розклад дзвінків завантажено"];
            }
            else
            {
                currentLoadAction = LOAD_END;
                [waitAlert setProgress:1.0 progressLabelText:@"Завантаження завершено"];
                [waitAlert dismissAnimated:YES completionHandler:nil];
                [self.button setEnabled:YES];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-M-d"];
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"Europe/Kiev"]];
                NSDate *date = [NSDate date];
                [defaults setObject:[dateFormat stringFromDate:date] forKey:@"updated_at"];
            }
        }
        else
        {
            if(currentLoadAction == LOAD_SUBJECTS)
            {
                currentLoadAction = LOAD_TEACHERS;
                [self UpdateData:TEACHER_URL];
                [updateWaitAlert setProgress:0.20 progressLabelText:@"Предмети завантажено"];
            }
            else if(currentLoadAction == LOAD_TEACHERS)
            {
                currentLoadAction = LOAD_AUDIENCE;
                [self UpdateData:AUDIENCE_URL];
                [updateWaitAlert setProgress:0.40 progressLabelText:@"Аудиторії завантажено"];
            }
            else if(currentLoadAction == LOAD_AUDIENCE)
            {
                currentLoadAction = LOAD_CAMPUS;
                [self UpdateData:CAMPUS_URL];
                [updateWaitAlert setProgress:0.60 progressLabelText:@"Корпуса завантажено"];
            }
            else if(currentLoadAction == LOAD_CAMPUS)
            {
                currentLoadAction = LOAD_TIME;
                [self UpdateData:TIME_URL];
                
                [updateWaitAlert setProgress:0.80 progressLabelText:@"Розклад дзвінків завантажено"];
            }
            else
            {
                currentLoadAction = LOAD_END;
                [updateWaitAlert setProgress:1.0 progressLabelText:@"Завантаження завершено"];
                [updateWaitAlert dismissAnimated:YES completionHandler:nil];
                [self.button setEnabled:YES];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-M-d"];
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"Europe/Kiev"]];
                NSDate *date = [NSDate date];
                [defaults setObject:[dateFormat stringFromDate:date] forKey:@"updated_at"];
            }
        }
    });
}

-(BOOL) Check
{
    NSString *databaseName = @"rozklad_db";
    
    NSString *documentsFolder          = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *databaseFullDocumentPath = [documentsFolder stringByAppendingPathComponent:databaseName];
    
    FMDatabase *database = [[FMDatabase alloc] initWithPath:databaseFullDocumentPath];
    
    [database open];
    BOOL status = YES;
    FMResultSet *rs = [database executeQuery:@"select count(*) from `time`;"];
    [rs next];
    status = status && [[rs stringForColumnIndex:0] intValue] > 0;
    rs = [database executeQuery:@"select count(*) from `teacher`;"];
    [rs next];
    status = status && [[rs stringForColumnIndex:0] intValue] > 0;
    rs = [database executeQuery:@"select count(*) from 'subjects';"];
    [rs next];
    status = status && [[rs stringForColumnIndex:0] intValue] > 0;
    rs = [database executeQuery:@"select count(*) from `campus`;"];
    [rs next];
    status = status && [[rs stringForColumnIndex:0] intValue] > 0;
    rs = [database executeQuery:@"select count(*) from `audience`;"];
    [rs next];
    status = status && [[rs stringForColumnIndex:0] intValue] > 0;
    
    [database close];
//    [self.view makeToast:[NSString stringWithFormat:@"%i", status] duration:5.0f position:CSToastPositionCenter];
    return status;
}

+ (BOOL)initDB {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"sqlite.db"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path])
    {
        NSString *bundle =  [[ NSBundle mainBundle] pathForResource:@"sqlite" ofType:@"db"];
        [fileManager copyItemAtPath:bundle toPath:path error:nil];
        return YES;
    }
    return NO;
}

@end
