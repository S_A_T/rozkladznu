//
//  RozkladController.h
//  rozkladznu
//
//  Created by Artem Shevchenko on 12.09.15.
//  Copyright (c) 2015 MITK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewcontroller.h"

@interface RozkladController : UITableViewController <SWRevealViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (strong) NSDictionary *contentDict;

- (void)LoadData:(NSString *) myUrl;
@end
