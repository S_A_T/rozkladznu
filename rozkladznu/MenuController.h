//
//  MenuController.h
//  rozkladznu
//
//  Created by Artem Shevchenko on 12.09.15.
//  Copyright (c) 2015 MITK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuController : UITableViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
