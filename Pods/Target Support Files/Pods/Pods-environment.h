
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 2
#define COCOAPODS_VERSION_MINOR_AFNetworking 6
#define COCOAPODS_VERSION_PATCH_AFNetworking 0

// AFNetworking/NSURLConnection
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLConnection
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLConnection 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLConnection 0

// AFNetworking/NSURLSession
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLSession
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLSession 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLSession 0

// AFNetworking/Reachability
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Reachability
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Reachability 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_Reachability 0

// AFNetworking/Security
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Security
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Security 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_Security 0

// AFNetworking/Serialization
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Serialization
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Serialization 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_Serialization 0

// AFNetworking/UIKit
#define COCOAPODS_POD_AVAILABLE_AFNetworking_UIKit
#define COCOAPODS_VERSION_MAJOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_UIKit 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_UIKit 0

// Crashlytics
#define COCOAPODS_POD_AVAILABLE_Crashlytics
#define COCOAPODS_VERSION_MAJOR_Crashlytics 3
#define COCOAPODS_VERSION_MINOR_Crashlytics 3
#define COCOAPODS_VERSION_PATCH_Crashlytics 4

// FMDB
#define COCOAPODS_POD_AVAILABLE_FMDB
#define COCOAPODS_VERSION_MAJOR_FMDB 2
#define COCOAPODS_VERSION_MINOR_FMDB 5
#define COCOAPODS_VERSION_PATCH_FMDB 0

// FMDB/common
#define COCOAPODS_POD_AVAILABLE_FMDB_common
#define COCOAPODS_VERSION_MAJOR_FMDB_common 2
#define COCOAPODS_VERSION_MINOR_FMDB_common 5
#define COCOAPODS_VERSION_PATCH_FMDB_common 0

// FMDB/standard
#define COCOAPODS_POD_AVAILABLE_FMDB_standard
#define COCOAPODS_VERSION_MAJOR_FMDB_standard 2
#define COCOAPODS_VERSION_MINOR_FMDB_standard 5
#define COCOAPODS_VERSION_PATCH_FMDB_standard 0

// Fabric
#define COCOAPODS_POD_AVAILABLE_Fabric
#define COCOAPODS_VERSION_MAJOR_Fabric 1
#define COCOAPODS_VERSION_MINOR_Fabric 5
#define COCOAPODS_VERSION_PATCH_Fabric 1

// Fabric/Base
#define COCOAPODS_POD_AVAILABLE_Fabric_Base
#define COCOAPODS_VERSION_MAJOR_Fabric_Base 1
#define COCOAPODS_VERSION_MINOR_Fabric_Base 5
#define COCOAPODS_VERSION_PATCH_Fabric_Base 1

// KSCrash/Recording
#define COCOAPODS_POD_AVAILABLE_KSCrash_Recording
#define COCOAPODS_VERSION_MAJOR_KSCrash_Recording 0
#define COCOAPODS_VERSION_MINOR_KSCrash_Recording 0
#define COCOAPODS_VERSION_PATCH_KSCrash_Recording 5

// KSCrash/Reporting/Filters/AppleFmt
#define COCOAPODS_POD_AVAILABLE_KSCrash_Reporting_Filters_AppleFmt
#define COCOAPODS_VERSION_MAJOR_KSCrash_Reporting_Filters_AppleFmt 0
#define COCOAPODS_VERSION_MINOR_KSCrash_Reporting_Filters_AppleFmt 0
#define COCOAPODS_VERSION_PATCH_KSCrash_Reporting_Filters_AppleFmt 5

// KSCrash/Reporting/Filters/Base
#define COCOAPODS_POD_AVAILABLE_KSCrash_Reporting_Filters_Base
#define COCOAPODS_VERSION_MAJOR_KSCrash_Reporting_Filters_Base 0
#define COCOAPODS_VERSION_MINOR_KSCrash_Reporting_Filters_Base 0
#define COCOAPODS_VERSION_PATCH_KSCrash_Reporting_Filters_Base 5

// LGAlertView
#define COCOAPODS_POD_AVAILABLE_LGAlertView
#define COCOAPODS_VERSION_MAJOR_LGAlertView 1
#define COCOAPODS_VERSION_MINOR_LGAlertView 0
#define COCOAPODS_VERSION_PATCH_LGAlertView 3

// SWRevealViewController
#define COCOAPODS_POD_AVAILABLE_SWRevealViewController
#define COCOAPODS_VERSION_MAJOR_SWRevealViewController 2
#define COCOAPODS_VERSION_MINOR_SWRevealViewController 3
#define COCOAPODS_VERSION_PATCH_SWRevealViewController 0

// Toast
#define COCOAPODS_POD_AVAILABLE_Toast
#define COCOAPODS_VERSION_MAJOR_Toast 2
#define COCOAPODS_VERSION_MINOR_Toast 4
#define COCOAPODS_VERSION_PATCH_Toast 0

// YandexMobileMetrica
#define COCOAPODS_POD_AVAILABLE_YandexMobileMetrica
#define COCOAPODS_VERSION_MAJOR_YandexMobileMetrica 2
#define COCOAPODS_VERSION_MINOR_YandexMobileMetrica 0
#define COCOAPODS_VERSION_PATCH_YandexMobileMetrica 0

// protobuf-c
#define COCOAPODS_POD_AVAILABLE_protobuf_c
#define COCOAPODS_VERSION_MAJOR_protobuf_c 1
#define COCOAPODS_VERSION_MINOR_protobuf_c 0
#define COCOAPODS_VERSION_PATCH_protobuf_c 1

